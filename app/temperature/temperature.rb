class Temperature
  def self.temperature
    url = 'http://api.openweathermap.org/data/2.5/forecast?id=3433955&appid=5db5e8492fcdad4118b934e58509ae2a'

    begin
      response = Faraday.get(url)
      data = JSON.parse(response.body)
      data['list'][0]['main']['temp'] - 273.15
    rescue Faraday::Error => e
      raise "Error al obtener datos de la API: #{e.message}"
    rescue JSON::ParserError => e
      raise "Error al parsear la respuesta JSON: #{e.message}"
    rescue StandardError => e
      raise "Error inesperado: #{e.message}"
    end
  end

  def self.handle_response
    temperature = self.temperature

    if temperature.to_i > 30
      'hace calor, ponete los cortos'
    elsif temperature.to_i < 10
      'ta fresco, ponete los largos'
    else
      'ta bien, dale tranqui'
    end
  end
end
